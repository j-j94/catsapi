# API CRUD CATS

Este proyecto contiene un pequeño crud que permite la creación, eliminación, consulta y actualización de la información de gatos.

Este proyecto consta de los siguientes módulos:

-controller
-service
-database

## Ejecucion
Para ejecutar el proyecto es necesario tener Node.js Instalado, MongoDb y tambien instalar las dependencias de cada uno de los módulos. Ingresando en cada modulo y ejecutando el comando ```npm i  ```. 

Para correr el proyecto ingresar al modulo controller y ejecutar la instrucción:

```DEBUG=* node index.js```

