const catsDatabaseQuerys = require('database').catQuerys
const debug = require('debug')('cats-service')

async function getCats(){
    try {
        let cats = await catsDatabaseQuerys.getCats()
        debug(cats)
        return cats
    } catch (error) {
        throw new Error(error)
    }
}

async function createCat(catData){
    try {
        let cat = await catsDatabaseQuerys.createCat(catData)
        debug(cat)
        return cat
    } catch (error) {
        throw new Error(error)
    }
}

async function getById(id){
    try {
        let cat = await catsDatabaseQuerys.getById(id)
        debug(cat)
        return cat
    } catch (error) {
     throw new Error(error)   
    }
}

async function updateCat(data){
    try {
        let param = { _id:data['_id']}
        //delete data['_id']
        //delete data['_v']
        debug(data)
        let catUpdated = await catsDatabaseQuerys.updateCat(param,data)
        debug(catUpdated)
        return catUpdated
    } catch (error) {
        throw new Error(error)
    }
}

async function deleteCat(id){
    try {
        let catDeleted = await catsDatabaseQuerys.deleteCat(id)
        debug(catDeleted)
        return catDeleted
    } catch (error) {
        debug(error)
        throw new Error(error)
    }
}

module.exports = {
    getCats,
    createCat,
    getById,
    updateCat,
    deleteCat
}