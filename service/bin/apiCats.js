const axios = require('axios')
const debug = require('debug')('service-api-cats')
const URL = 'https://api.thecatapi.com/v1/images/search'
const URLVOTE = 'https://api.thecatapi.com/v1/favourites'
const AUTH = 'f221c99b-304d-4404-b111-cbd3ddccf31a'

async function getImages(){
    try {
        let response = await makeRequest()
        return response
    } catch (error) {
        throw new Error()
    }
}

async function saveFavorite(id){
    try {
        let body = {
            image_id:id,
            sub_id:`${Date.now()}`
        }
        const response = await axios.post(URLVOTE,body,{headers: {
            "x-api-key": AUTH
        }})
        debug(response)
        return response
        
    } catch (error) {
        debug (error.message)
        throw new Error()
    }
}

async function getFavorites(){
    try {
        const response = await axios.get(URLVOTE,{headers: {
            "x-api-key": AUTH
        }})

        debug(response['data'])
        return response
    } catch (error) {
        debug(error)
        throw new Error(error)
    }
}

async function makeRequest(){
    try {
        const response = await axios.get(URL, {
            params: {
              limit: 10
            },
            headers: {
                Authorization: AUTH
            }
            
          })
        debug(response['data'])
        return response['data']
    } catch (error) {
        debug(error)
        throw new Error(error)
    }
}

module.exports = {
    getImages,
    saveFavorite,
    getFavorites
}