const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Cat = new Schema({
    name:String,
    type:String,
    age:String,
    photo:String
},{
    timestamps:false
})

const modelCat = mongoose.model('cat',Cat)

module.exports = {modelCat}