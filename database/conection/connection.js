const mongoose = require('mongoose')
const debug = require('debug')('database-connection')

function conectionDatabase() {
  let options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    keepAlive: true,
    keepAliveInitialDelay: 300000,
    auto_reconnect: true
  }
  let db = mongoose.connection
  db.once('open', function () {
    debug('MongoDB connection opened!')
  });
  db.on('reconnected', function () {
    debug('MongoDB reconnected!')
  });
  db.on('disconnected', function () {
    conectDatabase(options)
  });
  conectDatabase(options)
}

function conectDatabase(options) {
  mongoose.connect(`mongodb://localhost:27017/kafka`, options).then(
    () => {
      debug("succesfull conect")
    }
  ).catch(error => {
    debug(`error : ${error}`)

  })
}

module.exports = {
  conectionDatabase
}
