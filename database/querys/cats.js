const catModdel = require('../models/cats').modelCat
const debug = require('debug')('cat-db')

async function getCats(){
    try {
        let cats = await catModdel.find({})
        debug(cats)
        return cats
    } catch (error) {
        throw new Error(error)
    }
}

async function createCat(dataCat){
    try {
        let cat = await catModdel.create(dataCat)
        debug(cat)
        return cat
    } catch (error) {
        throw new Error(error)
    }
}

async function getById(id){
    try {
        let cat = await catModdel.findOne({_id:id})
        debug(cat)
        return cat
    } catch (error) {
        throw new Error(error)
    }
}

async function updateCat(params,fields){
    try {
        let catUpdated = await catModdel.update(params,fields)
        debug(catUpdated)    
        return catUpdated    
    } catch (error) {
        throw new Error(error)
    }
}

async function deleteCat(id){
    try {
        let catDeleted = await catModdel.deleteOne({_id:id})
        debug(catDeleted)
        return catDeleted
    } catch (error) {
        throw new Error(error)
    }
}

module.exports = {
    getCats,
    createCat,
    getById,
    updateCat,
    deleteCat
}