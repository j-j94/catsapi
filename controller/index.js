const express = require('express')
const cors = require('cors')
const debug = require('debug')('server')
const bodyParser = require('body-parser')
const routes = require('./router/configRouter')
const app = express()
app.use(bodyParser.json())
app.use(cors())
app.use(express.urlencoded())
app.use('/',routes.router)
app.listen(3000,()=>{
    debug('app is working!')
})





