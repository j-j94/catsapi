const service = require('service')

const catService = require('service').catsService
const debug = require('debug')('cat-controller')

async function createCat(req,res){
    try {
        let data = req.body
        debug(req.body)
        let catCreated = await catService.createCat(data)
        debug(catCreated)
        res.status(200).json({data:catCreated,message:"cat created succesfull"})
    } catch (error) {
        debug(error)
        res.status(400).json({message:"an error ocurred during creating cat"})
    }
}

async function getCats(req,res){
    try {
        let cats = await catService.getCats()
        debug(cats)
        res.status(200).json({data:cats,message:"retrieve cats succesfull"})
    } catch (error) {
        res.tatus(400).json({message:"error retrieving cats"})
    }
}

async function getCatById(req,res){
    try {
        let id = req.params.id
        debug(id)
        let cat = await catService.getById(id)
        debug(cat)
        res.status(200).json({data:cat,message:"retrieve cat succesfull"})
    } catch (error) {
        debug(error)
        res.status(400).json({message:"error retrieving cat"})
    }
}

async function updateCats(req,res){
    try {
        let data = req.body
        debug(data)
        let catUpdate = await catService.updateCat(data)
        debug(catUpdate)
        res.status(200).json({data:catUpdate,message:"update cat succesfull"})
    } catch (error) {
        debug(error)
        res.status(400).json({message:"error retrieving cat"})
    }
}

async function deleteCat(req,res){
    try {
        let id = req.params.id
        let catDeleted = await catService.deleteCat(id)
        debug(catDeleted)
        res.status(200).json({message:`succesful cat delete with id:  ${id}`})
    } catch (error) {
        res.status(400).json({message:"error deleting cat"})
    }
}


module.exports = {
    createCat,
    updateCats,
    getCats,
    getCatById,
    deleteCat
    
}