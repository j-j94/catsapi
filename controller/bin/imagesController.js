const debug = require('debug')('api-cats-controller')
const serviceApiCats = require('service').apiCatsOptions


async function getImages(req,res){
    try {
        let dataImages = await serviceApiCats.getImages()
        debug(dataImages)
        res.status(200).json({data:dataImages,message:"it works"})
    } catch (error) {
        res.status(400).json({message:"an error ocurred during fetching images"})
    }
}

async function getFavoriteImages(req,res){
    try {
       let images = await serviceApiCats.getFavorites()
       debug(images['data'])
       res.status(200).json({data:images['data'],message:"it works"})

    } catch (error) {
        debug(error)
        res.status(400).json({message:"an error ocurred during fetching images"})        
    }
}

async function saveFavorite(req,res){
    try {
        let id = `${req.params.id}`
        debug(id)
        let response = await serviceApiCats.saveFavorite(id)
        debug(dataImages)
        res.status(200).json({data:response['data'],message:"it works"})
        
    } catch (error) {
        debug(error)
        res.status(400).json({message:"an error ocurred saving image"})
    }
}

module.exports = {
    getImages,
    saveFavorite,
    getFavoriteImages
}