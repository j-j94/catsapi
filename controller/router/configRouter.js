let express = require('express')
let functions = require('../bin/imagesController')
let catFunctions = require('../bin/cats')
let debug = require('debug')('router-configuration')
let router = express.Router()

router.get('/imagenes',functions.getImages)
router.get('/imagenes/fav',functions.getFavoriteImages)
router.post('/imagenes/:id',functions.saveFavorite)
router.post('/gatos',catFunctions.createCat)
router.get('/gatos',catFunctions.getCats)
router.put('/gatos/:id',catFunctions.updateCats)
router.get('/gatos/:id',catFunctions.getCatById)
router.delete('/gatos/:id',catFunctions.deleteCat)

module.exports =  {router}